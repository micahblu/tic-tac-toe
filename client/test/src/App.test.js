import React from 'react'
import App from 'components/App'
import GameContainer from 'containers/GameContainer'
import configureStore from 'redux-mock-store'
import { startGame } from 'actions'
import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import { START_GAME } from '../../src/actions'

Enzyme.configure({ adapter: new Adapter() })

import ReactDOM from 'react-dom'

describe('App', () => {
  let store
  let props
  let container
  let wrapper 
  let mockStore
  let defaultState
  
  beforeEach(() => {
    mockStore = configureStore([])
    defaultState = {
      game: {
        isXNext: true,
        isGameOver: false,
        promptNames: false,
        winner: null,
        moves: 0,
        leaderBoardIsVisible: false,
        history: [new Array(9).fill(null)],
        playerOne: {
          "id": 1,
          "username": "Albert",
          "games": {
            "win": 5,
            "loss": 0,
            "draw": 1
          }
        },
        playerTwo: {
          "id": 2,
          "username": "Niels",
          "games": {
            "win": 4,
            "loss": 1,
            "draw": 1
          }
        }
      }
    }
  })

  it('Should dispatch START_GAME to store', () => {
    store = mockStore(defaultState)
    store.dispatch(startGame())
    let actions = store.getActions()
    expect(actions[0].type).to.equal(START_GAME)
  })
  it("Should show game intro with two inputs", () => {
    store = mockStore(Object.assign({}, defaultState, { game: Object.assign({}, defaultState.game, { isGameOver: true })}))
    let wrapper = mount(<Provider store={store}><GameContainer /></Provider>)
    expect(wrapper.find('input').length).to.equal(2)
  })
  it("Should show board when given players", () => {
    store = mockStore(defaultState)
    let wrapper = mount(<Provider store={store}><GameContainer /></Provider>)
    expect(wrapper.find('.board-container').length).to.equal(1)
  })
  it("Should show player's turn", () => {
    store = mockStore(defaultState)
    let wrapper = mount(<Provider store={store}><GameContainer /></Provider>)
    expect(wrapper.find('.current-player').text()).to.contain("Albert's turn")
  })
})
