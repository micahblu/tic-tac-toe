import React from 'react'
import App from 'components/App'
import GameContainer from 'containers/GameContainer'
import configureStore from 'redux-mock-store'
import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

import ReactDOM from 'react-dom'

describe('Leaderboard', () => {
  let store
  let props
  let container
  let wrapper 
  let mockStore
  let defaultState
  
  beforeEach(() => {
    mockStore = configureStore([])
    defaultState = {
      game: {
        isXNext: true,
        isGameOver: false,
        promptNames: false,
        winner: null,
        moves: 0,
        leaderBoardIsVisible: true,
        history: [new Array(9).fill(null)],
        leaderboard: [{
          "id": 1,
          "username": "Albert",
          "games": {
            "win": 5,
            "loss": 0,
            "draw": 1
          }
        },
        {
          "id": 2,
          "username": "Niels",
          "games": {
            "win": 4,
            "loss": 1,
            "draw": 1
          }
        }]
      }
    }
  })
  it("Should display leaderboard", () => {
    store = mockStore(defaultState)
    let wrapper = mount(<Provider store={ store }><GameContainer /></Provider>)
    expect(wrapper.find('.leaderboard').length).to.equal(1)
  })
  it("Should display the winner", () => {
    store = mockStore(Object.assign({}, defaultState, { game: Object.assign({}, defaultState.game, { winner: 'Niels' }) }))
    let wrapper = mount(<Provider store={ store }><GameContainer /></Provider>)
    expect(wrapper.find('.winner-board h2').text()).to.contain('Niels wins!')
  })
  it("Should display play again on game over", () => {
    store = mockStore(Object.assign({}, defaultState, { game: Object.assign({}, defaultState.game, { winner: 'Albert' }) }))
    let wrapper = mount(<Provider store={ store }><GameContainer /></Provider>)
    expect(wrapper.find('.play-again').text()).to.contain('Play Again?')
  })
})
