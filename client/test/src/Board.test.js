import React from 'react'
import Board from 'components/Board'
import Square from 'components/Square'
import { shallow } from 'enzyme'

describe('Board', () => {
  it('Loads with nine Square components', () => {
    const wrapper = shallow(<Board squares={new Array(9).fill(null)} />)
    //expect(wrapper.find(Foo)).to.have.length(3)
    expect(wrapper.find(Square)).to.have.length(9)
  })
})
