import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { createLogicMiddleware } from 'redux-logic'
import reducer from 'reducers'
import logic from 'logic'
import 'sanitize.css'
import 'font-awesome/css/font-awesome.min.css'  
import './styles/styles.css'

const middleware = createLogicMiddleware(logic)
const store = createStore(reducer, applyMiddleware(middleware))

store.subscribe(() => {
  console.log('state', store.getState())
})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, 
  document.getElementById('root')
)
  