import { createLogic } from 'redux-logic'
import { POST_OUTCOME, getLeaderboard } from 'actions'
import axios from 'axios'

export default createLogic({
  type: POST_OUTCOME,
  async process({ getState, action }, dispatch, done) {
    let playerOneResult
    let playerTwoResult
    if (action.payload.playerOne.id !== null) {
      playerOneResult = await axios.patch('http://localhost:3000/players/' + action.payload.playerOne.id, action.payload.playerOne)
    } else {
      playerOneResult = await axios.post('http://localhost:3000/players', action.payload.playerOne)
    }
    if (action.payload.playerTwo.id !== null) {
      playerTwoResult = await axios.patch('http://localhost:3000/players/' + action.payload.playerTwo.id, action.payload.playerTwo)
    } else {
      playerTwoResult = await axios.post('http://localhost:3000/players', action.payload.playerTwo)
    }
    dispatch(getLeaderboard({ show: false }))
    done()
  }
})