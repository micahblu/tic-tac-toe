import evaluateWinner from './evaluate-winner'
import getPlayers from './get-players'
import postOutcome from './post-outcome'
import getLeaderboard from './get-leaderboard'

export default [
  evaluateWinner,
  getPlayers,
  postOutcome,
  getLeaderboard
]