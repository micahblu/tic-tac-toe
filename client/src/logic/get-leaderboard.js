import { createLogic } from 'redux-logic'
import { GET_LEADERBOARD, updateLeaderboard, showLeaderboard } from 'actions'
import axios from 'axios'

export default createLogic({
  type: GET_LEADERBOARD,
  async process({ getState, action }, dispatch, done) {
    const response = await axios.get('http://localhost:3000/players')
    dispatch(updateLeaderboard(response.data))
    if (action.payload.show) dispatch(showLeaderboard())
    done()
  }
})