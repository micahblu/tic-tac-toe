import { createLogic } from 'redux-logic'
import { GET_PLAYERS, startGame } from 'actions'
import axios from 'axios'

const genPlayer = (name) => {
  return {
    "id": null,
    "username": name,
    "games": {
      "win": 0,
      "loss": 0,
      "draw": 0
    }
  }
}

export default createLogic({
  type: GET_PLAYERS,
  async process({ getState, action }, dispatch, done) {
    const playerOneResponse = await axios.get('http://localhost:3000/players?username=' + action.payload.playerOne)
    const playerTwoResponse = await axios.get('http://localhost:3000/players?username=' + action.payload.playerTwo)
   
    let playerOne = playerOneResponse.data[0] || genPlayer(action.payload.playerOne)
    let playerTwo = playerTwoResponse.data[0] || genPlayer(action.payload.playerTwo)

    dispatch(startGame(playerOne, playerTwo))
    done()
  }
})