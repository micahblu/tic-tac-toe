import { createLogic } from 'redux-logic'
import { EVALUATE_WINNER, declareWinner, getLeaders, postOutcome } from 'actions'
import axios from 'axios'

export default createLogic({
  type: EVALUATE_WINNER,
  process({ getState, action }, dispatch, done) {
    const state = getState()
    const squares = getState().game.history.slice(-1)[0]
    const winningLines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6]
    ]
    let winner = null
    let outcome = {}
    let playerOne = Object.assign({}, state.game.playerOne)
    let playerTwo = Object.assign({}, state.game.playerTwo)

    for (let i = 0, j = winningLines.length; i < j; i++) {
      const [a,b,c] = winningLines[i]
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        winner = squares[a]
        break
      }
    }
    // X is playerOne, O is playerTwo
    if (winner !== null) {
      if (winner === 'X') {
        playerOne.games.win += 1
        playerTwo.games.loss += 1
      } else {
        playerOne.games.loss += 1
        playerTwo.games.win += 1
      }
      dispatch(declareWinner((winner === 'X' ? playerOne : playerTwo).username))
      dispatch(postOutcome(playerOne, playerTwo))
    } else if (state.game.moves === 9) {
      dispatch(declareWinner('draw'))
      playerOne.games.draw += 1
      playerTwo.games.draw += 1
      dispatch(postOutcome(playerOne, playerTwo))
    }
    done()
  }
})