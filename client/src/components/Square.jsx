import React from 'react'

export default class Square extends React.Component {
  
  render({ onClick, symbol, index, position }=this.props) {
    
    const oIcon = <i className="fa o-icon hover fa-circle-o human" id="top-right" title="o"></i>
    const xIcon = <i className="fa x-icon fa-times" id="top-left" title="x"></i>

    let mark
    if (symbol) {
      mark = symbol === 'O' ? oIcon : xIcon
    }
    
    const positionClass = 'space ' + position 

    return (
      <div className="square" onClick={ e => onClick(index)}>
        <div className="square-inner">
          <div className="square-inner-inner">
            <div className={ positionClass }>{ mark }</div>
          </div>
        </div>
      </div>
    )

  }
}