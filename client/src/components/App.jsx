import React from 'react'
import ReactDOM from 'react-dom'
import { hot } from 'react-hot-loader'

import GameContainer from 'containers/GameContainer'

const App = () => {
  return <GameContainer />
}

export default hot(module)(App)
