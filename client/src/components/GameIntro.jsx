import React from 'react'

export default class GameIntro extends React.Component {
  constructor(props) {
    super(props)
    this.state = Object.assign({}, props)
    this.setName = this.setName.bind(this)
    this.onClick = this.onClick.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    this.setState(Object.assign({}, this.state, nextProps))
  }
  setName(player, val) {
    this.setState({
      errorMessage: null,
      [player]: val
    })
  }
  onClick(playerOne, playerTwo) {
    if (!playerOne || !playerTwo) {
      this.setState({
        errorMessage: 'Please enter player names'
      })
    } else if (playerOne === playerTwo) {
      this.setState({
        errorMessage: 'Player names must be unique'
      })
    } else {
      this.setState({
        errorMessage: null
      })
      this.state.onStartGame(playerOne, playerTwo)
    }
  }
  render({ isGameOver, playerOne, playerTwo, onStartGame, errorMessage } = this.state) {
    const style = isGameOver ? {display: 'block'} : { display: 'none'}
    let buttonClass = 'start-button ' + ((!playerOne || !playerTwo) || (playerOne === playerTwo)? 'disabled' : '')
    return (
      <div className="game-intro" style={ style }>
        <form onSubmit={ e => { e.preventDefault(); this.onClick(playerOne, playerTwo) } }>
          <div className="row">
            <label>
              Player one: <br />
              <input onChange={ evt => this.setName('playerOne', evt.target.value) } type="text" />
            </label>
          </div>
          <div className="row">
            <label>
              Player two: <br />
              <input onChange={ evt => this.setName('playerTwo', evt.target.value) } type="text" />
            </label>
          </div>
          { errorMessage ? <p className="danger">{ errorMessage }</p> : '' }
          
          <button type='submit' className={ buttonClass } onClick={ e => this.onClick(playerOne, playerTwo) }>Start Game!</button>
        </form>
      </div>
    )
  }
}