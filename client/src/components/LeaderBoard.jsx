import React from 'react'

export default class LeaderBoard extends React.Component {
  render({ players } = this.props) {
    if (players && players.length) {
      players.sort((a, b) => {
        if (a.games.win > b.games.win) return -1
        if (a.games.win < b.games.win) return 1
        if (a.games.loss > b.games.loss) return 1
        if (a.games.loss < b.games.loss) return -1
        return 0
      })
    }
    return (
      <div className="leaderboard">
        <h3>Leader board</h3>
        { !players && <div>Loading...</div> }
        <table>
          <thead>
            <tr>
              <th>Username</th>
              <th>Wins</th>
              <th>Losses</th>
              <th>Draws</th>
            </tr>
          </thead>
          <tbody>
          {
            players && players.map(player => {
              return (
                <tr key={ player.id }>
                  <td>{ player.username }</td>
                  <td>{ player.games.win }</td>
                  <td>{ player.games.loss }</td>
                  <td>{ player.games.draw }</td>
                </tr>
              )
            })
          }
          </tbody>
        </table>
      </div>
    )
  }
}