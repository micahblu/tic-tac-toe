import React from 'react'
import Square from './Square'

const getRow = index => {
  let row
  return index <= 2 ? "top" : ( index <= 5 ? "middle" : "bottom")
}

const getCol = index => {
  const leftCol = [0,3,6]
  const centerCol = [1,4,7]
  return leftCol.includes(index) ? "left" : (centerCol.includes(index) ? "center" : "right")
}

const getPosition = (index) => {
  return getRow(index) + '-' + getCol(index)
}

export default class Board extends React.Component {

  render({ onClick, squares } = this.props) {
    return (
      <div className="board-container">
        <ul className="grid board">
          {
            squares.map((symbol, index) => {
              const position = getPosition(index)
              return <li key={ index }><Square symbol={ symbol } onClick={ onClick } index={ index } position={ position } /></li>
            })
          }
        </ul>
      </div>
    )
  }
}