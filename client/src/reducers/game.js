import {
  START_GAME,
  END_GAME,
  DO_MOVE,
  DECLARE_WINNER,
  UPDATE_LEADERBOARD,
  SHOW_LEADERBOARD,
  HIDE_LEADERBOARD
} from '../actions'

const defaultState = {
  isXNext: true,
  isGameOver: true,
  promptNames: false,
  winner: null,
  moves: 0,
  leaderBoardIsVisible: false,
  history: [new Array(9).fill(null)]
}

const doMoveReducer = (action, state) => {
  const latestSquares = state.history.slice(-1)[0].slice()
  if (latestSquares[action.payload.index] === null) {
    latestSquares[action.payload.index] = state.isXNext ? 'X' : 'O'
    return Object.assign({}, state, {
      history: [].concat(state.history, [latestSquares]),
      isXNext: state.isXNext ? false : true,
      moves: state.moves += 1
    })
  } else {
    return state
  }
}

export default (state = defaultState, action) => {
  switch(action.type) {
    case START_GAME: return Object.assign({}, state, { isGameOver: false, playerOne: action.payload.playerOne, playerTwo: action.payload.playerTwo })
    case END_GAME: return Object.assign({}, defaultState)
    case DO_MOVE: return doMoveReducer(action, state)
    case UPDATE_LEADERBOARD: return Object.assign({}, state, { leaderboard: action.payload.players })
    case DECLARE_WINNER: return Object.assign({}, state, { winner: action.payload.winner})
    case SHOW_LEADERBOARD: return Object.assign({}, state, {leaderBoardIsVisible: true})
    case HIDE_LEADERBOARD: return Object.assign({}, state, {leaderBoardIsVisible: false})
    default: 
      return state
  }
}