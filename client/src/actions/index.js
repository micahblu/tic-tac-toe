// Action type constants

export const START_GAME = 'START_GAME'
export const END_GAME = 'END_GAME'
export const DO_MOVE = 'DO_MOVE'
export const EVALUATE_WINNER = 'EVALUATE_WINNER'
export const DECLARE_WINNER = 'DECLARE_WINNER'
export const GET_LEADERBOARD = 'GET_LEADERBOARD'
export const UPDATE_LEADERBOARD = 'UPDATE_LEADERBOARD'
export const SHOW_LEADERBOARD = 'SHOW_LEADERBOARD'
export const HIDE_LEADERBOARD = 'HIDE_LEADERBOARD'
export const POST_OUTCOME = 'POST_OUTCOME'
export const GET_PLAYERS = 'GET_PLAYERS'

// Action generators

export const startGame = (playerOne, playerTwo) => {
  return {
    type: START_GAME,
    payload: {
      playerOne,
      playerTwo
    }
  }
}

export const endGame = () => {
  return {
    type: END_GAME
  }
}

export const doMove = index => {
  return {
    type: DO_MOVE,
    payload: {
      index
    }
  }
}

export const evaluateWinner = squares => {
  return {
    type: EVALUATE_WINNER,
    payload: {
      squares
    }
  }
}

export const postOutcome = (playerOne, playerTwo) => {
  return {
    type: POST_OUTCOME,
    payload: {
      playerOne,
      playerTwo
    }
  }
}

export const getLeaderboard = ({show = false}) => {
  return {
    type: GET_LEADERBOARD,
    payload: {
      show
    }
  }
}

export const updateLeaderboard = players => {
  return {
    type: UPDATE_LEADERBOARD,
    payload: {
      players
    }
  }
}

export const showLeaderboard = () => {
  return {
    type: SHOW_LEADERBOARD
  }
}

export const hideLeaderboard = () => {
  return {
    type: HIDE_LEADERBOARD
  }
}

export const declareWinner = winner => {
  return {
    type: DECLARE_WINNER,
    payload: {
      winner
    }
  }
}

export const getPlayers = (playerOne, playerTwo) => {
  return {
    type: GET_PLAYERS,
    payload: {
      playerOne,
      playerTwo
    }
  }
}