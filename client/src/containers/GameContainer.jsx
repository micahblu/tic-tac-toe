import React from 'react'
import { connect } from 'react-redux'
import BoardContainer from './BoardContainer'
import LeaderBoardContainer from 'containers/LeaderBoardContainer'
import { startGame, endGame, getPlayers, getLeaderboard, hideLeaderboard } from 'actions'
import GameIntro from 'components/GameIntro'

const GameContainer = ({ currentPlayer, winner, isGameOver, onStartGame, onPlayAgain, showLeaderboard, hideLeaderboard, leaderBoardIsVisible }) => {
  const leaderBoardOverlay = (
    <div className="overlay">
      <div className="winner-board">
        <LeaderBoardContainer />
        <button className="close-leaderboard" onClick={ hideLeaderboard }>Close</button>
      </div>
    </div>
  )
  
  const game = (
    <div>
      { !winner ? <p className="current-player pull-left">{ currentPlayer ? currentPlayer.username : ''}'s turn</p> : '' }
      <p className="pull-right"><a onClick={ showLeaderboard }>Leader Board </a></p>
      <BoardContainer />
      { leaderBoardIsVisible && leaderBoardOverlay }  
    </div>
  )

  const displayWinner = (
    <div className="overlay">
      <div className="winner-board">
        { winner === 'draw' ? <h2>It's a draw!</h2> : <h2>{ winner } wins!</h2> }
        <LeaderBoardContainer />
        <button className="play-again" onClick={ onPlayAgain }>Play Again?</button>
      </div>
    </div>
  )

  return (
    <div className="game-container">
      <h2 className="game-title">Tic Tac Toe</h2>
      <GameIntro onStartGame={ onStartGame } isGameOver={ isGameOver } />
      { winner ? displayWinner : '' }
      { !isGameOver && game }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isGameOver: state.game.isGameOver,
    currentPlayer: state.game.isXNext ? state.game.playerOne : state.game.playerTwo,
    winner: state.game.winner,
    leaderBoardIsVisible: state.game.leaderBoardIsVisible
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onStartGame(playerOne, playerTwo) {
      dispatch(getPlayers(playerOne, playerTwo))
    },
    onPlayAgain() {
      dispatch(endGame())
    },
    showLeaderboard() {
      dispatch(getLeaderboard({ show: true}))
    },
    hideLeaderboard() {
      dispatch(hideLeaderboard())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameContainer)