import React from 'react'
import { connect } from 'react-redux'
import LeaderBoard from 'components/LeaderBoard'
import { hideLeaderboard } from 'actions'

const LeaderBoardContainer = ({ players }) => {
  return <LeaderBoard players={ players } />
}

const mapStateToProps = state => {
  return {
    players: state.game.leaderboard
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onClose() {
      dispatch(hideLeaderboard())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeaderBoardContainer)