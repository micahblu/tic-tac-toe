import React from 'react'
import { connect } from 'react-redux'
import Board from '../components/Board'
import { doMove, evaluateWinner } from 'actions'

const BoardContainer = ({ onClick, squares }) => {
  return <Board onClick={ onClick } squares={ squares } />
}

const mapStateToProps = state => {
  return {
    squares: state.game.history.slice(state.game.history.length-1)[0]
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onClick(index, squares) {
      dispatch(doMove(index))
      dispatch(evaluateWinner())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoardContainer)