const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: [
    'babel-polyfill',
    'webpack-hot-middleware/client',
    path.resolve(__dirname, './client/src/index.js')
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/static/'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, "client/src"), "node_modules"]
  },
  module: {
    rules: [{
      test: /\.(png|jpg|gif|woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8192
          }
        }
      ]
    }, {
      test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
      use: [
        {
          loader: 'file-loader',
          options: {}
        }
      ]
    },{
      test: /\.(js|jsx)$/,
      loader: require.resolve('babel-loader'),
      include: path.join(__dirname, 'client/src'),
      options: {
        presets: [
          ['@babel/preset-env', {
            'targets': {
              'browsers': ['last 2 versions', 'safari >= 7']
            }
          }],
          '@babel/preset-react'
        ],
        plugins: [
          'react-hot-loader/babel',
          '@babel/plugin-proposal-object-rest-spread',
          '@babel/plugin-proposal-class-properties'
        ]
      }
    }, {
      test: /\.css$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: "css-loader",
        options: { importLoaders: 1 }
      }, {
        loader: "postcss-loader",
        options: {
          plugins: loader => [
            require('postcss-cssnext')()
          ]
      }}]
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
}