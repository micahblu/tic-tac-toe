## Notes

# Getting started:
```js 
git clone git@bitbucket.org:micahblu/tic-tac-toe.git
cd tic-tac-toe
npm i
npm run dev
```

## Approach
I used Redux for the state container, redux-logic for the middleware logic and used the existing Karma/Enzyme for unit testing. Server side I used the RESTful api to POST, PATCH, and GET player data. Using PATCH requests I was able to make the player data truly persistent through many games. The styling is minimal yet effective and is responsive between smaller and larger displays.

### Closing remarks
This was a very fun project to build. I hope the work meets expectations. Please let me know if you have any questions. Thank you.

-Micah Blu
